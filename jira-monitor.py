#!/usr/bin/env python
import urllib2
import json
from  subprocess import PIPE, Popen
from time import sleep

JIRA_URL = 'http://localhost:8040/rest/api/2/serverInfo?doHealthCheck=true'
timeout = 30

def check():
    passed = True

    try:
        response = urllib2.urlopen(JIRA_URL, None, timeout)
        data = json.loads(response.read())

        for check in data.get('healthChecks', []):
            if not check.get('passed'):
                passed = False
    except Exception as e:
        passed = False
        print str(e)

    if not passed:
        p = Popen(['./support-data-noinput.sh'],stdin=PIPE,stdout=PIPE,stderr=PIPE)


if __name__ == "__main__":
    check()